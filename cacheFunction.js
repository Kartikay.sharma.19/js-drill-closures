const result = function cacheFunction(cb) {
    const cache = {};
    return (args) => {
        if(cache[args]){
    return cache[args];
        }else{
            cache[args] = cb(args);
            return cache[args];
        }

    }
}
module.exports = result;