module.exports = function(cb, n){
    if(n === undefined) {
        return null ;

    }
    else{
        return () => {
            if(n>0) {
                cb() ;
                n--;
            }
        }
    }
}